<?php
// récupération et sauvegarde d'une tâche
if (isset($_POST["content"]) && $_POST["content"] !== "") {
    $nomfichier = time().".txt";
    $content = $_POST["content"];
    file_put_contents($nomfichier, $content . "\n");
}
// récuperation de la liste de tache

// récupération nom fichier présents dans le dossier et ne garde que les .txt
$liste = [];
foreach (new DirectoryIterator('.') as $fileInfo) {
    if ($fileInfo->isDot()) continue; // filtre le . et le ..
    $rec = $fileInfo->getFilename(); // met les noms de fichiers dans le tableau rec
    $test = strpos($rec,'txt'); // ne retient que les fichiers avec l'extention txt
    if ($test) {
        $liste["$rec"] = file_get_contents($rec); // et les range dans le tableau liste
    }
}
// suppression d'une tâche
if (isset($_GET["line"])){
    $lineToDelete = $_GET["line"];
    unlink($lineToDelete); // supprime le fichier correspondant à la tache à supprimer
    // refait l'affichage (mise à jour)
    $liste = [];
    foreach (new DirectoryIterator('.') as $fileInfo) {
        if ($fileInfo->isDot()) continue;
        $rec = $fileInfo->getFilename();
        $test = strpos($rec,'txt');
        if ($test) {
            $liste["$rec"] = file_get_contents($rec);
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<ul>
<?php
// affichage des tâches
foreach ($liste as $index => $task) {
        echo "<li>" . $task . "<a href='liste_tache.php?line=".$index."'>[X]</a></li>";
    }
?>
</ul>

<!--formulaire de suppression-->
<form action="" method="get">
    <input type="number" name="line">
    <input type="submit" value="Supprimer">
</form>
<!--formulaire de création-->
<form action="liste_tache.php" method="post">
    <label for="content-input">Tache:</label>
    <input id="content-input" type="text" name="content">
    <input type="submit" value="Ajouter">
</form>
</body>
</html>
